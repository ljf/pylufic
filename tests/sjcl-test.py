#!/usr/bin/env python3

from json import loads, dumps
from sjcl import SJCL, sjcl
from base64 import b64decode, b64encode


if __name__ == "__main__":
    key = b64encode(sjcl.get_random_bytes(32))
    data = b"azertyuiopqsdfgfhjklmwxcvbn"
    encrypted = SJCL().encrypt(b64encode(data), key)
    for k in encrypted.keys():
        if isinstance(encrypted[k], bytes): # b64encode produces bytes
            encrypted[k] = encrypted[k].decode("ascii")
    jsonedjsoned = dumps(dumps(encrypted))
    dejsoned = loads(loads(jsonedjsoned))
    assert(data == b64decode(SJCL().decrypt(dejsoned, key)))
