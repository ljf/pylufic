#!/usr/bin/env python3

from websocket import create_connection


if __name__ == "__main__":
    ws = create_connection("wss://echo.websocket.org")
    mess = "azertyuiopqsdfgfhjklmwxcvbn"
    ws.send(mess)
    echo = ws.recv()
    ws.close()
    assert(mess == echo)